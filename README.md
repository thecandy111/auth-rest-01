# Pruebas js de criptografia y JWT

Para la siguiente práctica necesitaremos tres librerías nuevas que instalaremos desde la terminal con el comando npm:

```
npm i -S bcrypt
npm i -S moment
npm i -S jwt-simple
```

En primer lugar, tendremos que crear un certificado:
```
openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem
```
Y podríamos eliminar en csr.pem porque ya tenemos el certificado creado y este archivo es prácticamente inútil.


Ahora tendremos que crear un nuevo repositorio en nuestro BitBucket con los pasos que vimos en la práctica anterior. Después de eso, ya podemos empezar a crear archivos.js y escribir nuestro código.

Necesitaremos, principalmente, 3 archivos js, a los que he denominado de la siguiente forma: 01_bcryps.js, 03_pass-test.js y 04_jwt-test.js. 
Además, tendremos que crear una nueva carpeta dentro de esta, que sean los servicios. Dentro de ella, tendremos dos archivos más con extensión js: pass.service.js y token.service.js.

Trabajo realizado por Ana Martínez Hernández.