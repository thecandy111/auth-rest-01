'use strict'

const PassService = require('./services/pass.service');
const moment = require('moment');
const { hash } = require('bcrypt');

// Datos para simulacion...
const miPass = "miContraseña";
const badPass = "miOtraContraseña";
const usuario = {
    _id: '123456789',
    email: 'amh68@gcloud.ua.es',
    displayName: 'Ana Martínez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Encriptamos el password...
PassService.encriptaPassword( usuario.password )
    .then( hash => {
        usuario.password = hash;
        console.log(usuario);

        //Verificamos el pass
        PassService.comparaPassword( miPass, usuario.password)
            .then( isOk => {
                if(isOk){
                    console.log('p1: El pass es correcto');
                }else{
                    console.log('p1: El pass no es correcto');
                }
            })
            .catch(err => console.log(err));

        //Verificamos el pass
        PassService.comparaPassword( badPass, usuario.password)
        .then( isOk => {
            if(isOk){
                console.log('p2: El pass es correcto');
            }else{
                console.log('p2: El pass no es correcto');
            }
        })
        .catch(err => console.log(err));
    });
