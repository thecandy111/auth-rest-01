'use strict'

const TokenService = require('./services/token.service');
const moment = require('moment');

const miPass = "miContraseña";
const badPass = "miOtraContraseña";
const usuario = {
    _id: '123456789',
    email: 'amh68@gcloud.ua.es',
    displayName: 'Ana Martínez',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Creamos un token...
const token = TokenService.creaToken( usuario );
console.log(token);

//Decodificar un token...
TokenService.decodificaToken( token )
    .then(userId => {
        return console.log(`ID1: ${userId}`);
    })
    .catch(err => console.log(err));


//Decodificar un token erroneo...
const badToken = 'EyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJpbnQiOjE2NDc3MjkwNDYsImV4cCI6MTY0ODMzMzg0Nn0.I_dxXQSHqRkg514DynITKfVqx8l8M7pQJmgXJADGGUs';
TokenService.decodificaToken( badToken )
    .then(userId => {
        return console.log(`ID2: ${userId}`);
    })
    .catch(err => console.log(err));
