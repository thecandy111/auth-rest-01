'use strict'

const bcrypt = require('bcrypt');

//encriptaPassword
// Devuelve un hash con un salt incluido

function encriptaPassword( password ){
    return bcrypt.hash( password, 10);
}

//comparaPassword
//Devolver verdadero o falso si coinciden o no el pass y el hash

function comparaPassword( password, hash ){
    return bcrypt.compare( password, hash );
}

module.exports = {
    encriptaPassword,
    comparaPassword
};