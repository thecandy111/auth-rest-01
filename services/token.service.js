'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

// Crear token
// Devuelve un token tipo JWT
//      HEADER.PAYLOAD.VERIFY_SIGNATURE

//Donde:
//      HEADER( Objet JSON con el al...)
//      {
//      alg: ...
//...
//      VERYFY_SIGNATURE = HMACSHA256( base64UlrEncode(HEAD)+"."+ base64UlrEncode(PAYLOAD), SECRET)

function creaToken( user ){
    const payload = {
        sub: user._id,
        int: moment().unix(),
        exp: moment(). add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode(payload, SECRET);
}

// DecodificaToken
//develve el identificador del usuario

function decodificaToken( token ){
    return new Promise( (resolve, reject) => {
        try{
            const payload = jwt.decode(token, SECRET, true);
            if(payload.exp <= moment().unix()){
                reject({
                    status: 401,
                    message: 'El token ha caducado'
                });
            }
            console.log(payload);
            resolve( payload.sub );

        } catch {
            reject({
                status: 500,
                message: 'El token no es válido'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};